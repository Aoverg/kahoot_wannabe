# Kahoot-lookalike
In this project I made a GUI interface that will mimic the already popular quiz website Kahoot. The goal of this project is to get familliar with Qt-design tools and get used to working with GUI interface.

## Goal
The program takes inn a name and then leads the player through several questins wiht four possible answers where only one is correct. Thorughout the session the program wil keep track of points and answers that the players gets, and diplays this at the end of the game.

## Program used for this project
The program used for this project is the QT-Creator 8.0.1 (community). Should you run the program or make changes you would need to download the software from the website: https://www.qt.io/download-qt-installer.

## The game
the game is simple, it takes inn a player name before beginning the quiz. After typing the name you can either start the game using "enter" or pressing the start game button. when the game begins there is a timer at 15seconds that begins to count down. Trying to mimic the kahoot pints system i made it so the quicker you answer the question correctly the more points you get. after the game is done you are presented with a game compleat window that tells the player how many points they got with two buttons asking if they want to start over, or exit the application.

## Previewe images:
Start screen:
![alt text](/img/previewe_1.png)

Answering questions-screen
![alt text](/img/previewe_2.png)

Results screen
![alt text](/img/previewe_3.png)



## Creator
Alexander Øvergård