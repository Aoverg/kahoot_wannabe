#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void time_update();


private slots:
    void update_time();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void button_pressed();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void check_answer(int value);

    void start_game();

    void on_pushButton_7_clicked();

    void on_pushButton_6_clicked();

    void on_lineEdit_returnPressed();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
};

#endif // MAINWINDOW_H
