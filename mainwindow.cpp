#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <QDebug>

bool can_play = false;
int question = 0;
std::vector<int> answers = {0, 0, 0, 2, 0, 2};
int points = 0;
QString name;
int time_left;
bool playing = false;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);

    QFont font_1 = ui->label_4->font();
    font_1.setPointSize(20);
    ui->label_4->setFont(font_1);

    QFont font("Courier New");
    font.setStyleHint(QFont::Monospace);
    QApplication::setFont(font);

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::update_time);
    timer->start(1000);
}

void MainWindow::update_time()
{
    ui->lcdNumber->display(time_left);
    time_left -= 1;

    if(time_left < 0 && playing)
    {
        button_pressed();
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
   start_game();

}

void MainWindow::on_pushButton_2_clicked()
{
    check_answer(0);
    button_pressed();
}

void MainWindow::on_pushButton_3_clicked()
{
    check_answer(1);
    button_pressed();
}

void MainWindow::on_pushButton_4_clicked()
{
    check_answer(2);
    button_pressed();
}

void MainWindow::on_pushButton_5_clicked()
{
    check_answer(3);
    button_pressed();
}

void MainWindow::check_answer(int value){
    if(value == answers[question])
        {
        points += time_left;
        }
}

void MainWindow::button_pressed(){
    switch(question){
    case 0:
        ui->label_4->setText("How much water moves through nidelven per sekund?");
        ui->pushButton_2->setText("94700 liter"); //riktig
        ui->pushButton_3->setText("92400 liter");
        ui->pushButton_4->setText("87200 liter");
        ui->pushButton_5->setText("103900 liter");
        break;
    case 1:
        ui->label_4->setText("How long is nidelven?");
        ui->pushButton_2->setText("31.2km"); //riktig
        ui->pushButton_3->setText("23.6km");
        ui->pushButton_4->setText("39.5km");
        ui->pushButton_5->setText("18km");
        break;
    case 2:
        ui->label_4->setText("What country is nidelven located in?");
        ui->pushButton_2->setText("Sweden");
        ui->pushButton_3->setText("Denmark");
        ui->pushButton_4->setText("Norway"); //riktig
        ui->pushButton_5->setText("Finland");
        break;
    case 3:
        ui->label_4->setText("What is the biggest activity in Nidelven?");
        ui->pushButton_2->setText("Fishing"); //fishing
        ui->pushButton_3->setText("Surfing");
        ui->pushButton_4->setText("floating on inflateble boat");
        ui->pushButton_5->setText("Bathing");
        break;
    case 4:
        ui->label_4->setText("Nidelven passes one particular church, whitch one?");
        ui->pushButton_2->setText("Vikøy kyrkje");
        ui->pushButton_3->setText("Mariakirken");
        ui->pushButton_4->setText("Nidarosdomen"); //riktig
        ui->pushButton_5->setText("Borgund Stavkirke");
        break;
    default:
        ui->stackedWidget->setCurrentIndex(2);
        ui->label_7->setText(QString("%1, you got %2 points!").arg(name).arg(points));
        break;
    }
    question += 1;
    ui->label_6->setText(QString("Points: %1").arg(points));
    time_left = 15;

}
void MainWindow::on_pushButton_7_clicked()
{
    question = 0;
    points = 0;
    playing = false;
    ui->stackedWidget->setCurrentIndex(0);
}
void MainWindow::on_pushButton_6_clicked()
{
    QCoreApplication::exit(0);
}

void MainWindow::start_game()
{
    if(!ui->lineEdit->text().isEmpty())
    {
        name = ui->lineEdit->text();
        ui->label_5->setText(QString("Name: %1").arg(name));
        ui->stackedWidget->setCurrentIndex(1);
        playing = true;
        button_pressed();
    }
}

void MainWindow::on_lineEdit_returnPressed()
{
    start_game();
}

